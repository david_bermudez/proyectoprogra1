/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package proyecto.controller;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import proyecto.model.ClsPedidos;
import java.io.Serializable;

public class SrvPedidos extends ClsPedidos implements Serializable{
    private final String URL = "src"+File.separator+"archivos"+File.separator+"pedidos.dat";
    
    private File bd = null;
    private FileOutputStream fos;
    private ObjectOutputStream fileout;
    private FileInputStream fis;
    private ObjectInputStream filein;
    
    public File validarArchivo() {
    File bd = new File(URL);
    
    if (!bd.exists()) {
        try {
            //creo el archivo
            bd.createNewFile();
        } catch (IOException ex) {
            System.out.println("Error al accesar al archivo");
        }
    }

    return bd;
    }
    
    public int insertar(ClsPedidos pedido) {
        int res = 0;
        try {
            this.bd = this.validarArchivo();
            //abrir el archivo
            fos = new FileOutputStream(bd, true);

            //abro el flujo de escritura
            fileout = new ObjectOutputStream(fos);

            //escribir en el archivo
            fileout.writeObject(pedido);

        } catch (FileNotFoundException ex) {
            //res = 1;
        } catch (IOException ex) {
            res = 2;
            //Logger.getLogger(clsServicesEstudiante.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileout.close();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(ClsPedidos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return res;
    }
    
    public ArrayList<ClsPedidos> getRegistros() {
        ArrayList<ClsPedidos> pedidos = new ArrayList<>();

        try {
            this.bd = this.validarArchivo();

            this.fis = new FileInputStream(bd);

            while (true) {
                //por cada registro hay que instanciar un obj
                this.filein = new ObjectInputStream(fis);
                pedidos.add((ClsPedidos) filein.readObject());
            }

        } catch (EOFException ex) {

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClsPedidos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ClsPedidos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                filein.close();
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ClsPedidos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return pedidos;
    }
    
    public int eliminar(int numOrden) {
        int res = 0;
        ArrayList<ClsPedidos> pedidos = this.getRegistros();
        try {
            this.bd = this.validarArchivo();
            this.bd.delete();
            this.bd = this.validarArchivo();

            this.fos = new FileOutputStream(bd);

            for (ClsPedidos pedido : pedidos) {
                if (pedido.getNumeroOrden()!= numOrden) {
                    this.fileout = new ObjectOutputStream(fos);
                    fileout.writeObject(pedido);
                } else {
                }
            }
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(clsServicesEstudiante.class.getName()).log(Level.SEVERE, null, ex);
            res = 1;
        } catch (IOException ex) {
            res = 2;
        } finally {
            try {
                fileout.close();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(ClsPedidos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return res;
    }
    
    public int modificar(ClsPedidos orden) {
        int res = 0;
        ArrayList<ClsPedidos> pedidos = this.getRegistros();
        try {
            this.bd = this.validarArchivo();
            this.bd.delete();
            this.bd = this.validarArchivo();

            this.fos = new FileOutputStream(bd);

            for (ClsPedidos pedido : pedidos) {
                fileout = new ObjectOutputStream(fos);
                if (pedido.getCodigo() != orden.getCodigo()) {
                    fileout.writeObject(pedido);
                } else {
                    fileout.writeObject(orden);
                }
            }
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(clsServicesEstudiante.class.getName()).log(Level.SEVERE, null, ex);
            res = 1;
            System.out.println("Error 1");
        } catch (IOException ex) {
            res = 2;
            System.out.println("Error 2");
        } finally {
            try {
                fileout.close();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(ClsPedidos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return res;
    }
    
    private void imprimir(ArrayList<ClsPedidos> pedidos) {

        for (ClsPedidos pedido : pedidos) {
            System.out.println("Numero Orden: " + pedido.getNumeroOrden());
            System.out.println("---------------------------------------------------");
            System.out.println("Nombre: " + pedido.getPlato());
            System.out.println("Precio: " + pedido.getPrecio());
            System.out.println("Tipo: " + pedido.getTipo());
            System.out.println("---------------------------------------------------");
        }
    }
    
    public void consultar() {
        this.imprimir(this.getRegistros());
    }
    
    public ArrayList<ClsPedidos> consultarNumOrden(int numOrden) {
       ArrayList<ClsPedidos> pedidos = this.getRegistros();
       ArrayList<ClsPedidos> res = new ArrayList<>();

        for (ClsPedidos pedido : pedidos ) {
            if (pedido.getNumeroOrden() == numOrden) {
                res.add(pedido);
            }
        }
        return res;
    }
    
    public int calcularTotal(int numOrden) {
       ArrayList<ClsPedidos> pedidos = this.getRegistros();
       ArrayList<ClsPedidos> res = new ArrayList<>();
       int totalGlobal = 0;

        for (ClsPedidos pedido : pedidos ) {
            if (pedido.getNumeroOrden() == numOrden) {
                totalGlobal = totalGlobal + pedido.getTotal();
            }
        }
        return totalGlobal;
    }
}
