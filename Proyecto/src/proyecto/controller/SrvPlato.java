/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package proyecto.controller;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import proyecto.model.ClsPlato;

public class SrvPlato {
    private final String URL = "src"+File.separator+"archivos"+File.separator+"platos.dat";
    
    private File bd = null;
    private FileOutputStream fos;
    private ObjectOutputStream fileout;
    private FileInputStream fis;
    private ObjectInputStream filein;
    
    public File validarArchivo() {
    File bd = new File(URL);
    
    if (!bd.exists()) {
        try {
            //creo el archivo
            bd.createNewFile();
        } catch (IOException ex) {
            System.out.println("Error al accesar al archivo");
        }
    }

    return bd;
    }
    
    public int insertar(ClsPlato plato) {
        int res = 0;
        try {
            this.bd = this.validarArchivo();
            //abrir el archivo
            fos = new FileOutputStream(bd, true);

            //abro el flujo de escritura
            fileout = new ObjectOutputStream(fos);

            //escribir en el archivo
            fileout.writeObject(plato);

        } catch (FileNotFoundException ex) {
            //res = 1;
        } catch (IOException ex) {
            res = 2;
            //Logger.getLogger(clsServicesEstudiante.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileout.close();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(ClsPlato.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return res;
    }
    
    public ArrayList<ClsPlato> getRegistros() {
        ArrayList<ClsPlato> platos = new ArrayList<>();

        try {
            this.bd = this.validarArchivo();

            this.fis = new FileInputStream(bd);

            while (true) {
                //por cada registro hay que instanciar un obj
                this.filein = new ObjectInputStream(fis);
                platos.add((ClsPlato) filein.readObject());
            }

        } catch (EOFException ex) {

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClsPlato.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ClsPlato.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                filein.close();
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ClsPlato.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return platos;
    }
    
    public int eliminar(int codigo) {
        int res = 0;
        ArrayList<ClsPlato> platos = this.getRegistros();
        try {
            this.bd = this.validarArchivo();
            this.bd.delete();
            this.bd = this.validarArchivo();

            this.fos = new FileOutputStream(bd);

            for (ClsPlato plato : platos) {
                if (plato.getCodigo() != codigo) {
                    this.fileout = new ObjectOutputStream(fos);
                    fileout.writeObject(plato);
                } else {
                }
            }
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(clsServicesEstudiante.class.getName()).log(Level.SEVERE, null, ex);
            res = 1;
        } catch (IOException ex) {
            res = 2;
        } finally {
            try {
                fileout.close();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(ClsPlato.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return res;
    }
    
    public int modificar(ClsPlato platillo) {
        int res = 0;
        ArrayList<ClsPlato> platos = this.getRegistros();
        try {
            this.bd = this.validarArchivo();
            this.bd.delete();
            this.bd = this.validarArchivo();

            this.fos = new FileOutputStream(bd);

            for (ClsPlato plato : platos) {
                fileout = new ObjectOutputStream(fos);
                if (plato.getCodigo() != platillo.getCodigo()) {
                    fileout.writeObject(plato);
                } else {
                    fileout.writeObject(platillo);
                }
            }
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(clsServicesEstudiante.class.getName()).log(Level.SEVERE, null, ex);
            res = 1;
            System.out.println("Error 1");
        } catch (IOException ex) {
            res = 2;
            System.out.println("Error 2");
        } finally {
            try {
                fileout.close();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(ClsPlato.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return res;
    }
    
    private void imprimir(ArrayList<ClsPlato> platos) {

        for (ClsPlato plato : platos) {
            System.out.println("Codigo: " + plato.getCodigo());
            System.out.println("Nombre: " + plato.getPlato());
            System.out.println("Precio: " + plato.getPrecio());
            System.out.println("Tipo: " + plato.getTipo());
            System.out.println("---------------------------------------------------");
        }
    }
    
    public void consultar() {
        this.imprimir(this.getRegistros());
    }
    
    public ArrayList<ClsPlato> consultarNombre(String nombre) {
       ArrayList<ClsPlato> platos = this.getRegistros();
       ArrayList<ClsPlato> res = new ArrayList<ClsPlato>();

        for (ClsPlato plato : platos ) {
            if (plato.getPlato().toLowerCase().contains(nombre.toLowerCase())) {
                res.add(plato);
            }
        }
        return res;
    }
    
    public ArrayList<ClsPlato> consultarTipo(String nombre) {
       ArrayList<ClsPlato> platos = this.getRegistros();
       ArrayList<ClsPlato> res = new ArrayList<ClsPlato>();

        for (ClsPlato plato : platos ) {
            if (plato.getTipo().toLowerCase().contains(nombre.toLowerCase())) {
                res.add(plato);
            }
        }
        return res;
    }
}
