/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package proyecto.controller;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import proyecto.model.ClsUsuario;

public class SrvUsuario extends ClsUsuario implements Serializable{
    private final String URL = "src"+File.separator+"archivos"+File.separator+"usuarios.dat";
    
    private File bd = null;
    private FileOutputStream fos;
    private ObjectOutputStream fileout;
    private FileInputStream fis;
    private ObjectInputStream filein;
    
    public File validarArchivo() {
    File bd = new File(URL);
    
    if (!bd.exists()) {
        try {
            //creo el archivo
            bd.createNewFile();
        } catch (IOException ex) {
            System.out.println("Error al accesar al archivo");
        }
    }

    return bd;
    }
    
    public int insertar(ClsUsuario usuario) {
        int res = 0;
        try {
            this.bd = this.validarArchivo();
            //abrir el archivo
            fos = new FileOutputStream(bd, true);

            //abro el flujo de escritura
            fileout = new ObjectOutputStream(fos);

            //escribir en el archivo
            fileout.writeObject(usuario);

        } catch (FileNotFoundException ex) {
            //res = 1;
        } catch (IOException ex) {
            res = 2;
            //Logger.getLogger(clsServicesEstudiante.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileout.close();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(ClsUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return res;
    }
    
    public ArrayList<ClsUsuario> getRegistros() {
        ArrayList<ClsUsuario> usuarios = new ArrayList<>();

        try {
            this.bd = this.validarArchivo();

            this.fis = new FileInputStream(bd);

            while (true) {
                //por cada registro hay que instanciar un obj
                this.filein = new ObjectInputStream(fis);
                usuarios.add((ClsUsuario) filein.readObject());
            }

        } catch (EOFException ex) {

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClsUsuario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ClsUsuario.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                filein.close();
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ClsUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return usuarios;
    }
    
    public int logIn(String nombre, String password, String tipo) {
        int enc = 0;
        try {
            ArrayList<ClsUsuario> usuarios = this.getRegistros();
            ArrayList<ClsUsuario> res = new ArrayList<>();

            for (ClsUsuario usuario : usuarios) {
                if (usuario.getNombre().equals(nombre) && usuario.getPassword().equals(password) && usuario.getTipo().equals("admin")) {
                    res.add(usuario);
                    enc = 1;
                    break;
                }else if(usuario.getNombre().equals(nombre) && usuario.getPassword().equals(password) && (usuario.getTipo().equals("mesero"))){
                    res.add(usuario);
                    enc = 2;
                    break;
                }
            }
        } catch (Exception e) {
        }
        
        if(nombre.equals("superadmin") && password.equals("@dmin123!") && tipo.equals("admin")){
            enc = 1;
        }
        
        return enc;
    }
    
    public int eliminar(int codigo) {
        int res = 0;
        ArrayList<ClsUsuario> usuarios = this.getRegistros();
        try {
            this.bd = this.validarArchivo();
            this.bd.delete();
            this.bd = this.validarArchivo();

            this.fos = new FileOutputStream(bd);

            for (ClsUsuario usuario : usuarios) {
                if (usuario.getCodigo() != codigo) {
                    this.fileout = new ObjectOutputStream(fos);
                    fileout.writeObject(usuario);
                } else {
                }
            }
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(clsServicesEstudiante.class.getName()).log(Level.SEVERE, null, ex);
            res = 1;
        } catch (IOException ex) {
            res = 2;
        } finally {
            try {
                fileout.close();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(ClsUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return res;
    }
    
    public int modificar(ClsUsuario user) {
        int res = 0;
        ArrayList<ClsUsuario> usuarios = this.getRegistros();
        try {
            this.bd = this.validarArchivo();
            this.bd.delete();
            this.bd = this.validarArchivo();

            this.fos = new FileOutputStream(bd);

            for (ClsUsuario usuario : usuarios) {
                fileout = new ObjectOutputStream(fos);
                if (usuario.getCodigo() != user.getCodigo()) {
                    fileout.writeObject(usuario);
                } else {
                    fileout.writeObject(user);
                }
            }
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(clsServicesEstudiante.class.getName()).log(Level.SEVERE, null, ex);
            res = 1;
            System.out.println("Error 1");
        } catch (IOException ex) {
            res = 2;
            System.out.println("Error 2");
        } finally {
            try {
                fileout.close();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(ClsUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return res;
    }
    
    private void imprimir(ArrayList<ClsUsuario> usuarios) {

        for (ClsUsuario usuario : usuarios) {
            System.out.println("Usuario: " + usuario.getNombre());
            System.out.println("Tipo: " + usuario.getTipo());
            System.out.println("Password: " + usuario.getPassword());
            System.out.println("---------------------------------------------------");
        }
    }
    
    public void consultar() {
        this.imprimir(this.getRegistros());
    }
}
