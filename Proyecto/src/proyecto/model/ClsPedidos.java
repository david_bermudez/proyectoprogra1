/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package proyecto.model;

import java.io.Serializable;

public class ClsPedidos implements Serializable{
    private String plato, tipo;
    private int cantidad, precio, numeroOrden, codigo, total;

    public ClsPedidos() {
    }

    public ClsPedidos(String plato, String tipo, int cantidad, int precio, int numeroOrden, int codigo, int total) {
        this.plato = plato;
        this.tipo = tipo;
        this.cantidad = cantidad;
        this.precio = precio;
        this.numeroOrden = numeroOrden;
        this.codigo = codigo;
        this.total = total;
    }

    public String getPlato() {
        return plato;
    }

    public void setPlato(String plato) {
        this.plato = plato;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(int numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
    
    public int calcularPrecio() {
        return this.cantidad * this.precio;
    }
}
