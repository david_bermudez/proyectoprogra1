/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package proyecto.model;

import java.io.Serializable;

/**
 *
 * @author davidbermudez
 */
public class ClsPlato implements Serializable{
    
    private String plato, tipo;
    private int precio, codigo;

    public ClsPlato() {
    }

    public ClsPlato(String plato, String tipo, int codigo, int precio) {
        this.plato = plato;
        this.tipo = tipo;
        this.codigo = codigo;
        this.precio = precio;
    }

    public String getPlato() {
        return plato;
    }

    public void setPlato(String plato) {
        this.plato = plato;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
}
