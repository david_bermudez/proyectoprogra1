/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package proyecto.model;

import java.io.Serializable;

public class ClsUsuario implements Serializable{
    private String nombre, tipo, password;
    private int codigo;

    public ClsUsuario() {
    }


    public ClsUsuario(String nombre, String tipo, String password, int codigo) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.password = password;
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    
    public int validarPassword() {
        return this.password.matches("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,15})") ? 0 : 1;
    }
    
    public int validarNombre() {
        return this.nombre.matches("[a-zA-Z]+") ? 0 : 1;
    }
}