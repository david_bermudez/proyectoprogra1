/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JInternalFrame.java to edit this template
 */
package proyecto.view;

import java.awt.HeadlessException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import proyecto.controller.SrvPedidos;
import proyecto.controller.SrvPlato;
import proyecto.model.ClsPedidos;
import proyecto.model.ClsPlato;

/**
 *
 * @author davidbermudez
 */
public class FrmPedidos extends javax.swing.JInternalFrame {
    private SrvPlato servicePlatos;
    private int accion;
    /**
     * Creates new form FrmPedidos
     */
    public FrmPedidos() {
        accion = 1;
        servicePlatos = new SrvPlato();
        
        initComponents();
        
        txtPedidoCodigo.setEditable(false);
        txtPedidoPlato.setEditable(false);
        txtPedidoTipo.setEditable(false);
        txtPedidoPrecio.setEditable(false);
        
        try {
            loadTable(this.servicePlatos.getRegistros());
        } catch (Exception e) {
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupFilter = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        lblPedidoNumOrden = new javax.swing.JLabel();
        txtPedidoNumOrden = new javax.swing.JTextField();
        lblPedidoPlato = new javax.swing.JLabel();
        txtPedidoPlato = new javax.swing.JTextField();
        lblPedidoCantidad = new javax.swing.JLabel();
        txtPedidoCantidad = new javax.swing.JTextField();
        lblPedidoTipo = new javax.swing.JLabel();
        txtPedidoTipo = new javax.swing.JTextField();
        lblPedidoCodigo = new javax.swing.JLabel();
        txtPedidoCodigo = new javax.swing.JTextField();
        btnPedidoAgregar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        rdbtnPedidoFiltroNombre = new javax.swing.JRadioButton();
        rdbtnPedidoFiltroTipo = new javax.swing.JRadioButton();
        txtPedidoFilterSearch = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        btnPedidoCancelar = new javax.swing.JButton();
        lblPedidoPrecio = new javax.swing.JLabel();
        txtPedidoPrecio = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPedidos = new javax.swing.JTable();

        setClosable(true);
        setMaximizable(true);
        setTitle("Comanda");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Agregar Pedido", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 11), new java.awt.Color(0, 0, 0))); // NOI18N
        jPanel1.setForeground(new java.awt.Color(0, 0, 0));

        lblPedidoNumOrden.setFont(new java.awt.Font("Dialog", 2, 14)); // NOI18N
        lblPedidoNumOrden.setForeground(new java.awt.Color(0, 0, 0));
        lblPedidoNumOrden.setText("Orden #");

        txtPedidoNumOrden.setBackground(new java.awt.Color(255, 255, 255));
        txtPedidoNumOrden.setForeground(new java.awt.Color(0, 0, 0));
        txtPedidoNumOrden.setText("1");
        txtPedidoNumOrden.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtPedidoNumOrden.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPedidoNumOrdenActionPerformed(evt);
            }
        });

        lblPedidoPlato.setFont(new java.awt.Font("Dialog", 2, 14)); // NOI18N
        lblPedidoPlato.setForeground(new java.awt.Color(0, 0, 0));
        lblPedidoPlato.setText("Plato");

        txtPedidoPlato.setBackground(new java.awt.Color(255, 255, 255));
        txtPedidoPlato.setForeground(new java.awt.Color(0, 0, 0));
        txtPedidoPlato.setText("jTextField1");
        txtPedidoPlato.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblPedidoCantidad.setFont(new java.awt.Font("Dialog", 2, 14)); // NOI18N
        lblPedidoCantidad.setForeground(new java.awt.Color(0, 0, 0));
        lblPedidoCantidad.setText("Cantidad");

        txtPedidoCantidad.setBackground(new java.awt.Color(255, 255, 255));
        txtPedidoCantidad.setForeground(new java.awt.Color(0, 0, 0));
        txtPedidoCantidad.setText("jTextField1");
        txtPedidoCantidad.setToolTipText("");
        txtPedidoCantidad.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtPedidoCantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPedidoCantidadActionPerformed(evt);
            }
        });

        lblPedidoTipo.setBackground(new java.awt.Color(255, 255, 255));
        lblPedidoTipo.setFont(new java.awt.Font("Dialog", 2, 14)); // NOI18N
        lblPedidoTipo.setForeground(new java.awt.Color(0, 0, 0));
        lblPedidoTipo.setText("Tipo");

        txtPedidoTipo.setBackground(new java.awt.Color(255, 255, 255));
        txtPedidoTipo.setForeground(new java.awt.Color(0, 0, 0));
        txtPedidoTipo.setText("jTextField1");
        txtPedidoTipo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblPedidoCodigo.setFont(new java.awt.Font("Dialog", 2, 14)); // NOI18N
        lblPedidoCodigo.setForeground(new java.awt.Color(0, 0, 0));
        lblPedidoCodigo.setText("Codigo");

        txtPedidoCodigo.setBackground(new java.awt.Color(255, 255, 255));
        txtPedidoCodigo.setForeground(new java.awt.Color(0, 0, 0));
        txtPedidoCodigo.setText("jTextField1");
        txtPedidoCodigo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnPedidoAgregar.setBackground(new java.awt.Color(255, 255, 255));
        btnPedidoAgregar.setForeground(new java.awt.Color(0, 255, 0));
        btnPedidoAgregar.setText("Agregar");
        btnPedidoAgregar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btnPedidoAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPedidoAgregarActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 2, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Filtrar Por:");

        rdbtnPedidoFiltroNombre.setBackground(new java.awt.Color(255, 255, 255));
        btnGroupFilter.add(rdbtnPedidoFiltroNombre);
        rdbtnPedidoFiltroNombre.setForeground(new java.awt.Color(0, 0, 0));
        rdbtnPedidoFiltroNombre.setText("Nombre");

        rdbtnPedidoFiltroTipo.setBackground(new java.awt.Color(255, 255, 255));
        btnGroupFilter.add(rdbtnPedidoFiltroTipo);
        rdbtnPedidoFiltroTipo.setForeground(new java.awt.Color(0, 0, 0));
        rdbtnPedidoFiltroTipo.setText("Tipo de Plato");

        txtPedidoFilterSearch.setBackground(new java.awt.Color(255, 255, 255));
        txtPedidoFilterSearch.setForeground(new java.awt.Color(0, 0, 0));
        txtPedidoFilterSearch.setText("jTextField1");

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setForeground(new java.awt.Color(0, 0, 0));
        jButton1.setText("Filtrar");
        jButton1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        btnPedidoCancelar.setBackground(new java.awt.Color(255, 255, 255));
        btnPedidoCancelar.setForeground(new java.awt.Color(0, 0, 0));
        btnPedidoCancelar.setText("Cancelar");
        btnPedidoCancelar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btnPedidoCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPedidoCancelarActionPerformed(evt);
            }
        });

        lblPedidoPrecio.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        lblPedidoPrecio.setForeground(new java.awt.Color(0, 0, 0));
        lblPedidoPrecio.setText("Precio");

        txtPedidoPrecio.setBackground(new java.awt.Color(255, 255, 255));
        txtPedidoPrecio.setForeground(new java.awt.Color(0, 0, 0));
        txtPedidoPrecio.setText("jTextField1");
        txtPedidoPrecio.setToolTipText("");
        txtPedidoPrecio.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtPedidoPrecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPedidoPrecioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPedidoTipo)
                    .addComponent(lblPedidoCodigo)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtPedidoCodigo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .addComponent(txtPedidoTipo, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPedidoNumOrden, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPedidoNumOrden, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPedidoPlato, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPedidoPlato, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addComponent(lblPedidoCantidad)
                                .addGap(72, 72, 72)
                                .addComponent(lblPedidoPrecio))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(txtPedidoCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtPedidoPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jLabel1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(rdbtnPedidoFiltroNombre)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rdbtnPedidoFiltroTipo))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPedidoFilterSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnPedidoAgregar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addComponent(btnPedidoCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(82, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblPedidoNumOrden)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPedidoNumOrden, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPedidoPlato)
                    .addComponent(lblPedidoCantidad)
                    .addComponent(lblPedidoPrecio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                    .addComponent(txtPedidoCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPedidoPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPedidoPlato, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(lblPedidoTipo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPedidoTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblPedidoCodigo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPedidoCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rdbtnPedidoFiltroNombre)
                    .addComponent(rdbtnPedidoFiltroTipo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPedidoFilterSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPedidoAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPedidoCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Platos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 11), new java.awt.Color(0, 0, 0))); // NOI18N

        tblPedidos.setBackground(new java.awt.Color(255, 255, 255));
        tblPedidos.setForeground(new java.awt.Color(0, 0, 0));
        tblPedidos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Nombre", "Precio", "Tipo"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tblPedidos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblPedidosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblPedidos);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtPedidoCantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPedidoCantidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPedidoCantidadActionPerformed

    private void txtPedidoNumOrdenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPedidoNumOrdenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPedidoNumOrdenActionPerformed

    private void btnPedidoAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPedidoAgregarActionPerformed
        String dataMsg = null;
        ClsPedidos setPedido = new ClsPedidos();
        SrvPedidos srvPedido = new SrvPedidos();
        
        try {
            setPedido.setNumeroOrden(Integer.parseInt(txtPedidoNumOrden.getText()));
            try {
                setPedido.setPlato(txtPedidoPlato.getText());
                setPedido.setTipo(txtPedidoTipo.getText());
                
                try {
                    setPedido.setCantidad(Integer.parseInt(txtPedidoCantidad.getText()));
                    
                    try {
                        setPedido.setPrecio(Integer.parseInt(txtPedidoPrecio.getText()));
                        
                        try {
                            setPedido.setCodigo(Integer.parseInt(txtPedidoCodigo.getText()));
                            setPedido.setTotal(setPedido.calcularPrecio());
                            
                            srvPedido.insertar(setPedido);

                            dataMsg = "Plato Guardado Correctamente";
                            
                            JOptionPane.showMessageDialog(null, dataMsg, "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                            
                            limpiarDatos();
                        } catch (NumberFormatException e) {
                            JOptionPane.showMessageDialog(null, "Error Codigo debe ser numero entero", "Mensaje", JOptionPane.ERROR_MESSAGE);
                            txtPedidoCodigo.requestFocus();
                        }
                    } catch (NumberFormatException e) {
                        JOptionPane.showMessageDialog(null, "Error Precio debe ser numero entero", "Mensaje", JOptionPane.ERROR_MESSAGE);
                        txtPedidoPrecio.requestFocus();
                    }
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(null, "Error Cantidad debe ser numero entero", "Mensaje", JOptionPane.ERROR_MESSAGE);
                    txtPedidoCantidad.requestFocus();
                }
            } catch (HeadlessException e) {
            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Error Orden debe ser numero entero", "Mensaje", JOptionPane.ERROR_MESSAGE);
            txtPedidoNumOrden.requestFocus();
        }       
    }//GEN-LAST:event_btnPedidoAgregarActionPerformed

    private void tblPedidosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblPedidosMouseClicked
        txtPedidoCodigo.setText(this.tblPedidos.getValueAt(this.tblPedidos.getSelectedRow(),0).toString());
        txtPedidoPlato.setText(this.tblPedidos.getValueAt(this.tblPedidos.getSelectedRow(),1).toString());
        txtPedidoPrecio.setText(this.tblPedidos.getValueAt(this.tblPedidos.getSelectedRow(),2).toString());
        txtPedidoTipo.setText(this.tblPedidos.getValueAt(this.tblPedidos.getSelectedRow(),3).toString());
        txtPedidoNumOrden.requestFocus();
    }//GEN-LAST:event_tblPedidosMouseClicked

    private void btnPedidoCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPedidoCancelarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPedidoCancelarActionPerformed

    private void txtPedidoPrecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPedidoPrecioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPedidoPrecioActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        servicePlatos = new SrvPlato();
        if(rdbtnPedidoFiltroNombre.isSelected()){
            try {
                loadTable(servicePlatos.consultarNombre(txtPedidoFilterSearch.getText()));
            } catch (Exception e) {
            }  
        }
        
        if(rdbtnPedidoFiltroTipo.isSelected()){
            try {
                loadTable(servicePlatos.consultarTipo(txtPedidoFilterSearch.getText()));
            } catch (Exception e) {
            }  
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void loadTable(ArrayList<ClsPlato> platos) {
        DefaultTableModel modelTabla = (DefaultTableModel) this.tblPedidos.getModel();
        modelTabla.setRowCount(0);
        Object[] column = new Object[this.tblPedidos.getColumnCount()];
        
        for (ClsPlato plato : platos) {
            column[0] = plato.getCodigo();
            column[1] = plato.getPlato();
            column[2] = plato.getPrecio();
            column[3] = plato.getTipo();
            
            modelTabla.addRow(column);
        }
    }
    
    private void limpiarDatos() {
        txtPedidoCantidad.setText("");
        txtPedidoCodigo.setText("");
        txtPedidoFilterSearch.setText("");
        txtPedidoNumOrden.setText("");
        txtPedidoPlato.setText("");
        txtPedidoTipo.setText("");
        txtPedidoNumOrden.requestFocus();
        accion = 1;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup btnGroupFilter;
    private javax.swing.JButton btnPedidoAgregar;
    private javax.swing.JButton btnPedidoCancelar;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblPedidoCantidad;
    private javax.swing.JLabel lblPedidoCodigo;
    private javax.swing.JLabel lblPedidoNumOrden;
    private javax.swing.JLabel lblPedidoPlato;
    private javax.swing.JLabel lblPedidoPrecio;
    private javax.swing.JLabel lblPedidoTipo;
    private javax.swing.JRadioButton rdbtnPedidoFiltroNombre;
    private javax.swing.JRadioButton rdbtnPedidoFiltroTipo;
    private javax.swing.JTable tblPedidos;
    private javax.swing.JTextField txtPedidoCantidad;
    private javax.swing.JTextField txtPedidoCodigo;
    private javax.swing.JTextField txtPedidoFilterSearch;
    private javax.swing.JTextField txtPedidoNumOrden;
    private javax.swing.JTextField txtPedidoPlato;
    private javax.swing.JTextField txtPedidoPrecio;
    private javax.swing.JTextField txtPedidoTipo;
    // End of variables declaration//GEN-END:variables
}
